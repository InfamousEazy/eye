/*
 * This is the configuration file of the LegionBoard JavaScript client.
 * Copy this file or rename it to "configuration.js".
 *
 * @author	Nico Alt
 * @date	16.03.2016
 *
 * See the file "LICENSE" for the full license governing this code.
 */
var appConfig = {
	// Root URL of LegionBoard REST API (no slash at the end)
	'apiRoot': 'https://api.legionboard.example.com',
	// Title for LegionBoard
	'title': 'LegionBoard'
};
